<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supply extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'supply',
        'category_id',
        'unit_id',
        'price',
        'description',
        'is_active'
    ];

    protected function setSupplyAttribute($value){
        return $this->attributes['supply'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Units(){
        return $this->belongsTo(Unit::class,'unit_id','id');
    }

    public function Categories(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function Expenses(){
        return $this->hasMany(Expense::class,'supply_id','id');
    }

}
