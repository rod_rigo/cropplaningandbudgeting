<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seed extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'seed',
        'days',
        'price',
        'description',
        'is_active'
    ];

    protected function setSeedAttribute($value){
        return $this->attributes['seed'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Plants(){
        return $this->hasMany(Plant::class,'seed_id','id');
    }

}
