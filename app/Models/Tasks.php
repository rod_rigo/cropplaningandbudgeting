<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tasks extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'plant_id',
        'task',
        'start_at',
        'is_complete'
    ];

    protected function setTaskAttribute($value){
        return $this->attributes['task'] = ucwords($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Plants(){
        return $this->belongsTo(Plant::class,'plant_id','id');
    }

}
