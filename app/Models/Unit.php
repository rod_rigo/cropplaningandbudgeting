<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'unit',
        'is_active'
    ];

    protected function setUnitAttribute($value){
        return $this->attributes['unit'] = ucwords($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Supplies(){
        return $this->hasMany(Supply::class,'unit_id','id');
    }

}
