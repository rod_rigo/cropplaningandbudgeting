<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'is_admin',
        'is_active',
        'token',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'token',
        'remember_token',
    ];

    protected function setPasswordAttribute($value){
        return $this->attributes['password'] = Hash::make($value);
    }

    public function Seeds(){
        return $this->hasMany(Seed::class,'user_id','id');
    }

    public function Units(){
        return $this->hasMany(Unit::class,'user_id','id');
    }

    public function Categories(){
        return $this->hasMany(Category::class,'user_id','id');
    }

    public function Supplies(){
        return $this->hasMany(Supply::class,'user_id','id');
    }

    public function Plants(){
        return $this->hasMany(Plant::class,'user_id','id');
    }

    public function Tasks(){
        return $this->hasMany(Tasks::class,'user_id','id');
    }

    public function Expenses(){
        return $this->hasMany(Expense::class,'user_id','id');
    }

}
