<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'plant_id',
        'supply_id',
        'balance',
        'quantity',
        'price',
        'total',
        'total_balance',
        'remarks',
        'is_task'
    ];

    protected function setRemarksAttribute($value){
        return $this->attributes['remarks'] = ucwords($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Plants(){
        return $this->belongsTo(Plant::class,'plant_id','id');
    }

    public function Supplies(){
        return $this->belongsTo(Supply::class,'supply_id','id');
    }

}
