<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plant extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'seed_id',
        'budget',
        'quantity',
        'price',
        'total',
        'balance',
        'description',
        'is_active',
        'harvest_at',
        'harvested_at',
        'is_harvested'
    ];

    protected $dates = [
        'harvest_at',
        'harvested_at',
    ];

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Seeds(){
        return $this->belongsTo(Seed::class,'seed_id','id');
    }

    public function Tasks(){
        return $this->hasMany(Tasks::class,'plant_id','id');
    }

    public function Expenses(){
        return $this->hasMany(Expense::class,'plant_id','id');
    }

}
