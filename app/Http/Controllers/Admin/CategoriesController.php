<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Supply;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Categories.index');
    }

    public function getCategories(){
        $data = Category::withoutTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Categories.bin');
    }

    public function getCategoriesDeleted(){
        $data = Category::onlyTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $category = Category::query()->make($request->all());

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'category' => ['required', 'max:255', Rule::unique('categories','category')],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'category.required' => ucwords('please fill out this field'),
                'category.max' => ucwords('this field must not exceed at 255 characters'),
                'category.unique' => ucwords('this category is already exists'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            if($category->save()){
                $result = ['message' => ucwords('the category has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.categories.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the category has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Categories.add');
    }

    public function edit($id = null, Request $request){
        $category = Category::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'category' => ['required', 'max:255', Rule::unique('categories','category')->ignore($category->id)],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'category.required' => ucwords('please fill out this field'),
                'category.max' => ucwords('this field must not exceed at 255 characters'),
                'category.unique' => ucwords('this category is already exists'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            $category->update($request->all());

            if($category->save()){
                $result = ['message' => ucwords('the category has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.categories.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the category has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return response()->json($category);
    }

    public function delete($id = null){
        $category = Category::withoutTrashed()->findOrFail($id);

        $supply = Supply::withTrashed()
            ->where([
                ['category_id', '=', intval($category->id)]
            ])
            ->get()
            ->last();

        if(!empty($supply)){
            $result = ['message' => ucwords('the category has been constrained to supplies'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($category->delete()){
            $result = ['message' => ucwords('the category has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the category has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $category = Category::onlyTrashed()->findOrFail($id);

        if($category->restore()){
            $result = ['message' => ucwords('the category has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the category has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $category = Category::onlyTrashed()->findOrFail($id);

        $supply = Supply::withTrashed()
            ->where([
                ['category_id', '=', intval($category->id)]
            ])
            ->get()
            ->last();

        if(!empty($supply)){
            $result = ['message' => ucwords('the category has been constrained to supplies'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($category->forceDelete()){
            $result = ['message' => ucwords('the category has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the category has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
