<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supply;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnitsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Units.index');
    }

    public function getUnits(){
        $data = Unit::withoutTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Units.bin');
    }

    public function getUnitsDeleted(){
        $data = Unit::onlyTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $unit = Unit::query()->make($request->all());

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'unit' => ['required', 'max:255', Rule::unique('units','unit')],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'unit.required' => ucwords('please fill out this field'),
                'unit.max' => ucwords('this field must not exceed at 255 characters'),
                'unit.unique' => ucwords('this unit is already exists'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            if($unit->save()){
                $result = ['message' => ucwords('the unit has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.units.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the unit has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Units.add');
    }

    public function edit($id = null, Request $request){
        $unit = Unit::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'unit' => ['required', 'max:255', Rule::unique('units','unit')->ignore($unit->id)],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'unit.required' => ucwords('please fill out this field'),
                'unit.max' => ucwords('this field must not exceed at 255 characters'),
                'unit.unique' => ucwords('this unit is already exists'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            $unit->update($request->all());

            if($unit->save()){
                $result = ['message' => ucwords('the unit has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.units.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the unit has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return response()->json($unit);
    }

    public function delete($id = null){
        $unit = Unit::withoutTrashed()->findOrFail($id);

        $supply = Supply::withTrashed()
            ->where([
                ['unit_id', '=', intval($unit->id)]
            ])
            ->get()
            ->last();

        if(!empty($supply)){
            $result = ['message' => ucwords('the unit has been constrained to supplies'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($unit->delete()){
            $result = ['message' => ucwords('the unit has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the unit has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $unit = Unit::onlyTrashed()->findOrFail($id);

        if($unit->restore()){
            $result = ['message' => ucwords('the unit has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the unit has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $unit = Unit::onlyTrashed()->findOrFail($id);

        $supply = Supply::withTrashed()
            ->where([
                ['unit_id', '=', intval($unit->id)]
            ])
            ->get()
            ->last();

        if(!empty($supply)){
            $result = ['message' => ucwords('the unit has been constrained to supplies'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($unit->forceDelete()){
            $result = ['message' => ucwords('the unit has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the unit has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
