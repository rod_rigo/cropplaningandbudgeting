<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Users.index');
    }

    public function getUsers(){
        $data = User::withoutTrashed()->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Users.bin');
    }

    public function getUsersDeleted(){
        $data = User::onlyTrashed()->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $user = User::query()->make($request->all());

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username')],
                'email' => ['required', 'email', 'max:255', Rule::unique('users','email')],
                'password' => ['required', 'max:255', 'min:6'],
                'confirm_password' => ['required', 'max:255', 'same:password', 'min:6'],
                'is_admin' => ['required', 'numeric', Rule::in([0, 1])],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.email' => ucwords('please enter valid email'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.unique' => ucwords('this email is already exists'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'password.min' => ucwords('this field must have 6 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('confirm password do not matched'),
                'confirm_password.min' => ucwords('this field must have 6 characters'),
                'is_admin.required' => ucwords('please select is admin'),
                'is_admin.numeric' => ucwords('please select is admin'),
                'is_admin.in' => ucwords('please select is admin'),
                'is_active.required' => ucwords('please select is active'),
                'is_active.numeric' => ucwords('please select is active'),
                'is_active.in' => ucwords('please select is active'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->token = uniqid().uniqid();

            if($user->save()){
                $result = ['message' => ucwords('the user has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the user has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Users.add');
    }

    public function edit($id = null, Request $request){
        $user = User::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username')->ignore($user->id)],
                'email' => ['required', 'email', 'max:255', Rule::unique('users','email')->ignore($user->id)],
                'password' => ['required', 'max:255', 'min:6'],
                'confirm_password' => ['required', 'max:255', 'same:password', 'min:6'],
                'is_admin' => ['required', 'numeric', Rule::in([0, 1])],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.email' => ucwords('please enter valid email'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.unique' => ucwords('this email is already exists'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'password.min' => ucwords('this field must have 6 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('confirm password do not matched'),
                'confirm_password.min' => ucwords('this field must have 6 characters'),
                'is_admin.required' => ucwords('please select is admin'),
                'is_admin.numeric' => ucwords('please select is admin'),
                'is_admin.in' => ucwords('please select is admin'),
                'is_active.required' => ucwords('please select is active'),
                'is_active.numeric' => ucwords('please select is active'),
                'is_active.in' => ucwords('please select is active'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->token = uniqid().uniqid();

            if($user->save()){
                $result = ['message' => ucwords('the user has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the user has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Users.edit', compact('user'));
    }

    public function delete($id = null){
        $user = User::withoutTrashed()->findOrFail($id);
        if($user->delete()){
            $result = ['message' => ucwords('the user has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the user has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $user = User::onlyTrashed()->findOrFail($id);
        if($user->restore()){
            $result = ['message' => ucwords('the user has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the user has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $user = User::onlyTrashed()->findOrFail($id);
        if($user->forceDelete()){
            $result = ['message' => ucwords('the user has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the user has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
