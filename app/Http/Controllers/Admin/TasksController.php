<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tasks;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TasksController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTasks($plantId = null){
        $data = Tasks::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['plant_id', '=' => intval($plantId)]
            ])
            ->orderBy('start_at','ASC')
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required' ,'numeric', Rule::exists('users','id')],
                'plant_id' => ['required' ,'numeric', Rule::exists('plants','id')],
                'task' => ['required', 'max:4294967295'],
                'start_at' => ['required', 'date'],
                'is_complete' => ['required', 'numeric', Rule::in([0, 1])]
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'plant_id.required' => ucwords('please select plant id'),
                'plant_id.numeric' => ucwords('please select plant id'),
                'plant_id.exists' => ucwords('please select plant id'),
                'task.required' => ucwords('please fill out this field'),
                'task.max' => ucwords('this field mus not exceed at 4294967295 characters'),
                'is_complete.required' => ucwords('please select is complete'),
                'is_complete.numeric' => ucwords('please select is complete'),
                'is_complete.in' => ucwords('please select is complete')
            ]);

            $task = Tasks::query()->make($request->all());

            if($task->save()){
                $result = ['message' => ucwords('the task has been saved'), 'result' => ucwords('success')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the task has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function isComplete($id = null, $isComplete = null){
        $task = Tasks::withTrashed()->findOrFail($id);
        $statuses = [ucwords('Uncomplete'), ucwords('Complete')];
        $task->is_complete = intval($isComplete);

        if($task->save()){
            $result = ['message' => ucwords('the task has been '.($statuses[intval($isComplete)])), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the task has not been '.($statuses[intval($isComplete)])), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function delete($id = null){
        $task = Tasks::withTrashed()->findOrFail($id);

        if($task->forceDelete()){
            $result = ['message' => ucwords('the task has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the task has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

    }

}
