<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\Plant;
use App\Models\Seed;
use App\Models\Tasks;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PlantsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Plants.index');
    }

    public function getPlants(){
        $data = Plant::withoutTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            },
            'Seeds' => function($query){
                return $query->withTrashed()->get();
            },
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Plants.bin');
    }

    public function getPlantsDeleted(){
        $data = Plant::onlyTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            },
            'Seeds' => function($query){
                return $query->withTrashed()->get();
            },
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $plant = Plant::query()->make($request->all());

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'seed_id' => ['required', 'numeric', Rule::exists('seeds','id')],
                'budget' => ['required', 'numeric'],
                'quantity' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'total' => ['required', 'numeric'],
                'balance' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
                'harvest_at' => ['required', 'date'],
                'harvested_at' => ['nullable', 'date'],
                'is_harvested' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'seed_id.required' => ucwords('please select a seed'),
                'seed_id.numeric' => ucwords('please select a seed'),
                'seed_id.exists' => ucwords('please select a seed'),
                'budget.required' => ucwords('please fill out this field'),
                'budget.numeric' => ucwords('this field must be numeric value'),
                'quantity.required' => ucwords('please fill out this field'),
                'quantity.numeric' => ucwords('this field must be numeric value'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'total.required' => ucwords('please fill out this field'),
                'total.numeric' => ucwords('this field must be numeric value'),
                'balance.required' => ucwords('please fill out this field'),
                'balance.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please enter a description'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value'),
                'harvest_at.required' => ucwords('please fill out this field'),
                'harvest_at.date' => ucwords('this field must be date value'),
                'harvested_at.date' => ucwords('this field must be date value'),
                'is_harvested.required' => ucwords('please select is harvested value'),
                'is_harvested.numeric' => ucwords('please select is harvested value'),
                'is_harvested.in' => ucwords('please select is harvested value')
            ]);

            if($plant->save()){
                $result = ['message' => ucwords('the plant has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.plants.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the plant has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        $seeds = Seed::withoutTrashed()
            ->orderBy('seed', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->seed), 'key' => intval($query->id)];
            })->pluck('value', 'key');
        return view('Admin.Plants.add', compact('seeds'));
    }

    public function edit($id = null, Request $request){
        $plant = Plant::withoutTrashed()
            ->with([
                'Seeds' => function($query){
                    return $query->withTrashed()->get();
                },
                'Tasks' => function($query){
                    return $query->withTrashed()->get();
                },
                'Expenses' => function($query){
                    return $query->withTrashed()->get();
                },
            ])
            ->findOrFail($id);

        if($request->isMethod('post')){

            if(count($plant->tasks) || count($plant->expenses)){
                $result = ['message' => ucwords('the plant is already in progress'), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'seed_id' => ['required', 'numeric', Rule::exists('seeds','id')],
                'budget' => ['required', 'numeric'],
                'quantity' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'total' => ['required', 'numeric'],
                'balance' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
                'harvest_at' => ['required', 'date'],
                'harvested_at' => ['nullable', 'date'],
                'is_harvested' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'seed_id.required' => ucwords('please select a seed'),
                'seed_id.numeric' => ucwords('please select a seed'),
                'seed_id.exists' => ucwords('please select a seed'),
                'budget.required' => ucwords('please fill out this field'),
                'budget.numeric' => ucwords('this field must be numeric value'),
                'quantity.required' => ucwords('please fill out this field'),
                'quantity.numeric' => ucwords('this field must be numeric value'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'total.required' => ucwords('please fill out this field'),
                'total.numeric' => ucwords('this field must be numeric value'),
                'balance.required' => ucwords('please fill out this field'),
                'balance.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please enter a description'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value'),
                'harvest_at.required' => ucwords('please fill out this field'),
                'harvest_at.date' => ucwords('this field must be date value'),
                'harvested_at.date' => ucwords('this field must be date value'),
                'is_harvested.required' => ucwords('please select is harvested value'),
                'is_harvested.numeric' => ucwords('please select is harvested value'),
                'is_harvested.in' => ucwords('please select is harvested value')
            ]);

            $plant->update($request->all());

            if($plant->save()){
                $result = ['message' => ucwords('the plant has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.plants.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the plant has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        $seeds = Seed::withoutTrashed()
            ->orderBy('seed', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->seed), 'key' => intval($query->id)];
            })->pluck('value', 'key');
        return view('Admin.Plants.edit', compact('plant', 'seeds'));
    }

    public function view($id){
        $plant = Plant::withoutTrashed()->findOrFail($id);
        $seeds = Seed::withoutTrashed()->get()
            ->map(function ($query){
                return ['value' => ucwords($query->seed), 'key' => intval($query->id)];
            })->pluck('value', 'key');
        return view('Admin.Plants.view', compact('plant', 'seeds'));
    }

    public function delete($id = null){
        $plant = Plant::withoutTrashed()->findOrFail($id);

        $expense = Expense::withTrashed()
            ->where([
                ['plant_id', '=', intval($plant->id)]
            ])
            ->get()
            ->last();

        $task = Tasks::withTrashed()
            ->where([
                ['plant_id', '=', intval($plant->id)]
            ])
            ->get()
            ->last();

        if(!empty($expense)){
            $result = ['message' => ucwords('the plant has been constrained to expenses'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if(!empty($task)){
            $result = ['message' => ucwords('the plant has been constrained to tasks'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($plant->delete()){
            $result = ['message' => ucwords('the plant has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the plant has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $plant = Plant::onlyTrashed()->findOrFail($id);

        if($plant->restore()){
            $result = ['message' => ucwords('the plant has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the plant has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $plant = Plant::onlyTrashed()->findOrFail($id);

        $expense = Expense::withTrashed()
            ->where([
                ['plant_id', '=', intval($plant->id)]
            ])
            ->get()
            ->last();

        $task = Tasks::withTrashed()
            ->where([
                ['plant_id', '=', intval($plant->id)]
            ])
            ->get()
            ->last();

        if(!empty($expense)){
            $result = ['message' => ucwords('the plant has been constrained to expenses'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if(!empty($task)){
            $result = ['message' => ucwords('the plant has been constrained to tasks'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($plant->forceDelete()){
            $result = ['message' => ucwords('the plant has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the plant has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
