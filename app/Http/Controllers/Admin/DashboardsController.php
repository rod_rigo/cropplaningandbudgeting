<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plant;
use App\Models\Seed;
use App\Models\Supply;
use App\Models\Tasks;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Moment\Moment;

class DashboardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Dashboards.index');
    }

    public function counts(){
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $seeds = Seed::withoutTrashed()->get()->count();
        $supplies = Supply::withoutTrashed()->get()->count();
        $plants = Plant::withoutTrashed()->get()->count();
        $tasks = Tasks::withoutTrashed()
            ->where([
                ['start_at', '>=', $now]
            ])
            ->get()
            ->count();

        $collection = (new Collection([
            [
                'seeds' => doubleval($seeds),
                'supplies' => doubleval($supplies),
                'plants' => doubleval($plants),
                'tasks' => doubleval($tasks)
            ]
        ]));

        return response()->json($collection->first());
    }

    public function getTasks(){
        $tasks = Tasks::withoutTrashed()
            ->with([
                'Plants' => function($query){
                    return $query->withTrashed()->get();
                },
                'Plants.Seeds' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();

        return response()->json($tasks);
    }

}
