<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\Plant;
use App\Models\Supply;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ExpensesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getExpenses($plantId = null){
        $data = Expense::withoutTrashed()
            ->with([
                'Supplies' => function($query){
                    return $query->withTrashed()->get();
                },
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['plant_id', '=', intval($plantId)]
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $plant = Plant::withoutTrashed()->findOrFail($request->query('plantId'));
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'plant_id' => ['required', 'numeric', Rule::exists('plants','id')],
                'supply_id' => ['required', 'numeric', Rule::exists('supplies','id')],
                'balance' => ['required', 'numeric'],
                'quantity' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'total' => ['required', 'numeric'],
                'total_balance' => ['required', 'numeric'],
                'remarks' => ['required', 'max:4294967295'],
                'is_task' => ['required', 'numeric', Rule::in([0, 1])]
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'plant_id.required' => ucwords('please select a plant'),
                'plant_id.numeric' => ucwords('please select a plant'),
                'plant_id.exists' => ucwords('please select a plant'),
                'supply_id.required' => ucwords('please enter a supply'),
                'supply_id.numeric' => ucwords('please enter a supply'),
                'supply_id.exists' => ucwords('please enter a supply'),
                'balance.required' => ucwords('please fill out this field'),
                'balance.numeric' => ucwords('this field must be numeric value'),
                'quantity.required' => ucwords('please fill out this field'),
                'quantity.numeric' => ucwords('this field must be numeric value'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'total.required' => ucwords('please fill out this field'),
                'total.numeric' => ucwords('this field must be numeric value'),
                'total_balance.required' => ucwords('please fill out this field'),
                'total_balance.numeric' => ucwords('this field must be numeric value'),
                'remarks.required' => ucwords('please fill out this field'),
                'remarks.max' => ucwords('this field must nox exceed at 4294967295 characters'),
                'is_task.required' => ucwords('please select is task'),
                'is_task.numeric' => ucwords('please select is task'),
                'is_task.in' => ucwords('please select is task'),
            ]);

            $expense = Expense::query()->make($request->all());
            $supply = Supply::withoutTrashed()->findOrFail($request->input('supply_id'));

            $total = doubleval($supply->price) * intval($request->input('quantity'));
            $balance = $plant->balance;
            $total_balance = doubleval($balance) - doubleval($total);

            $expense->price = doubleval($supply->price);
            $expense->total = doubleval($total);
            $expense->balance = doubleval($balance);
            $expense->total_balance = doubleval($total_balance);

            $plant->balance = doubleval($total_balance);

            if($expense->save() && $plant->save()){
                $result = ['message' => ucwords('the expense has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.plants.view',['id' => intval($plant->id)])];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the expense has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Expenses.add', compact('plant'));
    }

    public function delete($id = null){
        $expense = Expense::withTrashed()->findOrFail($id);
        $plant = Plant::withoutTrashed()->findOrFail($expense->plant_id);

        $total = $expense->total;
        $balance = doubleval($plant->balance) + doubleval($total);

        $plant->balance = doubleval($balance);

        if($expense->forceDelete() && $plant->save()){
            $result = ['message' => ucwords('the expense has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the expense has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
