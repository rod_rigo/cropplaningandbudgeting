<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Expense;
use App\Models\Supply;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SuppliesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Supplies.index');
    }

    public function getSupplies(){
        $data = Supply::withoutTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            },
            'Categories' => function($query){
                return $query->withTrashed()->get();
            },
            'Units' => function($query){
                return $query->withTrashed()->get();
            },
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Supplies.bin');
    }

    public function getSuppliesDeleted(){
        $data = Supply::onlyTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            },
            'Categories' => function($query){
                return $query->withTrashed()->get();
            },
            'Units' => function($query){
                return $query->withTrashed()->get();
            },
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function getSuppliesList(){
        $data = Supply::withoutTrashed()
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get();
        return response()->json($data);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $supply = Supply::query()->make($request->all());

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'supply' => ['required', 'max:255', Rule::unique('supplies','supply')],
                'category_id' => ['required', 'numeric', Rule::exists('categories','id')],
                'unit_id' => ['required', 'numeric', Rule::exists('units','id')],
                'price' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'supply.required' => ucwords('please fill out this field'),
                'supply.max' => ucwords('this field must not exceed at 255 characters'),
                'supply.unique' => ucwords('this supply is already exists'),
                'category_id.required' => ucwords('please select category'),
                'category_id.numeric' => ucwords('please select category'),
                'category_id.exists' => ucwords('please select category'),
                'unit_id.required' => ucwords('please select unit'),
                'unit_id.numeric' => ucwords('please select unit'),
                'unit_id.exists' => ucwords('please select unit'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please enter a description'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            if($supply->save()){
                $result = ['message' => ucwords('the supply has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.supplies.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the supply has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        $categories = Category::withoutTrashed()
            ->orderBy('category', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->category), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        $units = Unit::withoutTrashed()
            ->orderBy('unit', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->unit), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Supplies.add', compact('categories', 'units'));
    }

    public function edit($id = null, Request $request){
        $supply = Supply::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'supply' => ['required', 'max:255', Rule::unique('supplies','supply')->ignore($supply->id)],
                'category_id' => ['required', 'numeric', Rule::exists('categories','id')],
                'unit_id' => ['required', 'numeric', Rule::exists('units','id')],
                'price' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'supply.required' => ucwords('please fill out this field'),
                'supply.max' => ucwords('this field must not exceed at 255 characters'),
                'supply.unique' => ucwords('this supply is already exists'),
                'category_id.required' => ucwords('please select category'),
                'category_id.numeric' => ucwords('please select category'),
                'category_id.exists' => ucwords('please select category'),
                'unit_id.required' => ucwords('please select unit'),
                'unit_id.numeric' => ucwords('please select unit'),
                'unit_id.exists' => ucwords('please select unit'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please enter a description'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            $supply->update($request->all());

            if($supply->save()){
                $result = ['message' => ucwords('the supply has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.supplies.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the supply has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        $categories = Category::withoutTrashed()
            ->orderBy('category', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->category), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        $units = Unit::withoutTrashed()
            ->orderBy('unit', 'ASC')
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->get()
            ->map(function ($query){
                return ['value' => ucwords($query->unit), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Supplies.edit', compact('supply', 'categories', 'units'));
    }

    public function delete($id = null){
        $supply = Supply::withoutTrashed()->findOrFail($id);

        $expense = Expense::withTrashed()
            ->where([
                ['supply_id', '=', intval($supply->id)]
            ])
            ->get()
            ->last();

        if(!empty($expense)){
            $result = ['message' => ucwords('the supply has been constrained to expenses'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($supply->delete()){
            $result = ['message' => ucwords('the supply has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the supply has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $supply = Supply::onlyTrashed()->findOrFail($id);

        if($supply->restore()){
            $result = ['message' => ucwords('the supply has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the supply has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $supply = Supply::onlyTrashed()->findOrFail($id);

        $expense = Expense::withTrashed()
            ->where([
                ['supply_id', '=', intval($supply->id)]
            ])
            ->get()
            ->last();

        if(!empty($expense)){
            $result = ['message' => ucwords('the supply has been constrained to expenses'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($supply->forceDelete()){
            $result = ['message' => ucwords('the supply has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the supply has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
