<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Plant;
use App\Models\Seed;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SeedsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('Admin.Seeds.index');
    }

    public function getSeeds(){
        $data = Seed::withoutTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        return view('Admin.Seeds.bin');
    }

    public function getSeedsDeleted(){
        $data = Seed::onlyTrashed()->with([
            'Users' => function($query){
                return $query->withTrashed()->get();
            }
        ])->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $seed = Seed::query()->make($request->all());

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'seed' => ['required', 'max:255', Rule::unique('seeds','seed')],
                'days' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'seed.required' => ucwords('please fill out this field'),
                'seed.max' => ucwords('this field must not exceed at 255 characters'),
                'seed.unique' => ucwords('this seed is already exists'),
                'days.required' => ucwords('please fill out this field'),
                'days.numeric' => ucwords('this field must be numeric value'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please fill out this field'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            if($seed->save()){
                $result = ['message' => ucwords('the seed has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.seeds.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the seed has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Seeds.add');
    }

    public function edit($id = null, Request $request){
        $seed = Seed::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'seed' => ['required', 'max:255', Rule::unique('seeds','seed')->ignore($seed->id)],
                'days' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
                'description' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([0, 1])],
            ],[
                'user_id.required' => ucwords('no user authenticated'),
                'user_id.numeric' => ucwords('no user authenticated'),
                'user_id.exists' => ucwords('no user authenticated'),
                'seed.required' => ucwords('please fill out this field'),
                'seed.max' => ucwords('this field must not exceed at 255 characters'),
                'seed.unique' => ucwords('this seed is already exists'),
                'days.required' => ucwords('please fill out this field'),
                'days.numeric' => ucwords('this field must be numeric value'),
                'price.required' => ucwords('please fill out this field'),
                'price.numeric' => ucwords('this field must be numeric value'),
                'description.required' => ucwords('please fill out this field'),
                'description.max' => ucwords('this field must not exceed at 4294967295 characters'),
                'is_active.required' => ucwords('please select is active value'),
                'is_active.numeric' => ucwords('please select is active value'),
                'is_active.in' => ucwords('please select is active value')
            ]);

            $seed->update($request->all());

            if($seed->save()){
                $result = ['message' => ucwords('the seed has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.seeds.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the seed has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        return view('Admin.Seeds.edit', compact('seed'));
    }

    public function view($id){
        $seed = Seed::withoutTrashed()->findOrFail($id);
        return response()->json($seed);
    }

    public function delete($id = null){
        $seed = Seed::withoutTrashed()->findOrFail($id);

        $plant = Plant::withTrashed()
            ->where([
                ['seed_id', '=', intval($seed->id)]
            ])
            ->get()
            ->last();

        if(!empty($plant)){
            $result = ['message' => ucwords('the seed has been constrained to plants'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($seed->delete()){
            $result = ['message' => ucwords('the seed has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the seed has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $seed = Seed::onlyTrashed()->findOrFail($id);

        if($seed->restore()){
            $result = ['message' => ucwords('the seed has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the seed has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $seed = Seed::onlyTrashed()->findOrFail($id);

        $plant = Plant::withTrashed()
            ->where([
                ['seed_id', '=', intval($seed->id)]
            ])
            ->get()
            ->last();

        if(!empty($plant)){
            $result = ['message' => ucwords('the seed has been constrained to plants'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }

        if($seed->forceDelete()){
            $result = ['message' => ucwords('the seed has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the seed has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
