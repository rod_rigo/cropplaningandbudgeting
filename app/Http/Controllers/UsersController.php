<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')
            ->except([
                'login',
                'logout'
            ]);
    }

    public function login(Request $request){
        if (!empty(auth()->user())) {
            return redirect()->route('users.logout');
        }

        if ($request->isMethod('post')) {

            $validator = $request->validate([
                'username' => ['required'],
                'password' => ['required']
            ],[
                'username.required' => ucwords('this field is required'),
                'password.required' => ucwords('this field is required')
            ]);

            $auth = User::query()->where(function ($query) use ($request){
                return $query->where('users.username','like','%'.$request->input('username').'%')
                    ->orWhere('users.email','like','%'.$request->input('username').'%');
            })->count();

            if ($auth > 0) {

                $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
                if (auth()->attempt([$field => $request->input('username'), 'password' => $request->input('password'), 'is_active' => 1],$request->boolean('remember_me'))) {
                    $request->session()->regenerate();
                    $user = User::query()->findOrFail(auth()->id());
                    $user->password = $request->input('password');
                    $user->token = uniqid().uniqid();
                    if ($user->save()) {
                        if(boolval(auth()->user()->is_admin)){
                            $result = ['message' => ucwords('authentication success'), 'resulr' => ucwords('success'),
                                'redirect' => route('admin.users.index')];
                            return response()->json($result,200);
                        }
                    }else{
                        if(boolval(auth()->user()->is_admin)){
                            $result = ['message' => ucwords('authentication success'), 'resulr' => ucwords('success'),
                                'redirect' => route('admin.users.index')];
                            return response()->json($result,200);
                        }
                    }

                }else{
                    $result = ['message' => ucwords('please check your credentials'), 'resulr' => ucwords('error')];
                    return response()->json($result,422);
                }

            }else{
                $result = ['message' => ucwords('please check your credentials'), 'resulr' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Users.login');
    }

    public function logout(Request $request)
    {
        if(!empty(auth()->user())){
            auth()->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect()->route('users.login');
        }
        return redirect()->route('users.login');
    }

}
