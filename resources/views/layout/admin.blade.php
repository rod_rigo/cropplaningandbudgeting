<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{url('public/assets/css/bootstrap/css/bootstrap.min.css')}}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{url('public/assets/pages/waves/css/waves.min.css')}}" type="text/css" media="all">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{url('public/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{url('public/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{url('public/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('public/assets/css/jquery.mCustomScrollbar.css')}}">

    <link rel="stylesheet" href="{{url('public/jquery/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{url('public/print-js/css/print.css')}}">
    <link rel="stylesheet" href="{{url('public/evo-calendar/css/evo-calendar.css')}}">
    <link rel="stylesheet" href="{{url('public/evo-calendar/css/evo-calendar.midnight-blue.css')}}">
    <link rel="stylesheet" href="{{url('public/i-check/css/icheck-bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('public/select2/css/select2.css')}}">

    {{--<link rel="stylesheet" href="{{url('public/datatables/css/jquery.dataTables.min.css')}}">--}}
    <link rel="stylesheet" href="{{url('public/datatables/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/buttons/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/buttons/buttons.bootstrap4.min.css')}}">

    <script src="{{url('public/jquery/js/jquery-3.6.0.js')}}"></script>
    <script src="{{url('public/jquery/js/jquery-ui.js')}}"></script>
    <script src="{{url('public/moment/js/moment.js')}}"></script>
    <script src="{{url('public/ck-editor/js/ckeditor.js')}}"></script>
    <script src="{{url('public/sweet-alert/js/sweetalert2.js')}}"></script>
    <script src="{{url('public/sweet-alert/js/sweetalert2.all.js')}}"></script>
    <script src="{{url('public/turbo-links/js/turbolinks.js')}}"></script>
    <script src="{{url('public/excel-js/js/exceljs.js')}}"></script>
    <script src="{{url('public/print-js/js/print.js')}}"></script>
    <script src="{{url('public/filesaver-js/js/FileSaver.js')}}"></script>
    <script src="{{url('public/evo-calendar/js/evo-calendar.js')}}"></script>
    <script src="{{url('public/chartjs/js/chart.js')}}"></script>
    <script src="{{url('public/chartjs/js/chartjs-plugin-autocolors.js')}}"></script>
    <script src="{{url('public/select2/js/select2.js')}}"></script>

    <script src="{{url('public/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/datatables/js/colorder/ColReorderWithResize.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{url('public/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.print.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/html2pdf.bundle.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/vfs_fonts.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/pdfmake.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/jszip.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.html5.min.js')}}"></script>

    <script>
        var mainurl = window.location.origin+'/cropplaningandbudgeting/admin/';
    </script>

</head>
<style>
    #description {
        width: 100%;
        margin:auto;
    }
    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 18em;
    }
    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
    .select2-selection.select2-selection--single{
        line-height: 1.25;
        padding: .2rem .3rem;
        border-radius: 0;
        height: 34px;
    }
    .select2-selection__arrow{
        margin-top: 0.2em;
    }
</style>
<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="loader-track">
        <div class="preloader-wrapper">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        @include('element.admin.navbar')

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                @include('element.admin.sidebar')
                <div class="pcoded-content">
                    <!-- Page-header start -->
                   @include('element.admin.header')
                    <!-- Page-header end -->
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <div class="page-body">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="styleSelector">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{url('public/assets/images/browser/chrome.png')}}" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{url('public/assets/images/browser/firefox.png')}}" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{url('public/assets/images/browser/opera.png')}}" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{url('public/assets/images/browser/safari.png')}}" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{url('public/assets/images/browser/ie.png')}}" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="{{url('public/assets/js/popper.js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/assets/js/bootstrap/js/bootstrap.min.js')}} "></script>
<!-- waves js -->
<script src="{{url('public/assets/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{url('public/assets/js/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{url('public/assets/js/SmoothScroll.js')}}"></script>
<script src="{{url('public/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{url('public/assets/js/pcoded.min.js')}}"></script>
<script src="{{url('public/assets/js/vertical-layout.min.js')}} "></script>
<script src="{{url('public/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{url('public/assets/js/script.js')}}"></script>

<script>

    $('input[type="number"]').on('keypress', function (e) {
        var key = e.key;
        var regex = /^([0-9]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    });

    function capitalize(sentence) {
        return sentence.replace(/\b\w/g, function(char) {
            return char.toUpperCase();
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

    function toast(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            toast: true,
            position: 'top-right',
            timerProgressBar:true,
        })
    }
</script>

</body>
</html>