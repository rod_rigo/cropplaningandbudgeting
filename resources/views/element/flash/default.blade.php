@if(session('error'))
    <script>
        Swal.fire({
            icon: 'error',
            title: null,
            text: '{{session('error')}}',
            timer: 5000,
            timerProgressBar:true,
        });
    </script>
@elseif(session('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: null,
            text: '{{session('success')}}',
            timer: 5000,
            timerProgressBar:true,
        });
    </script>
@else

@endif