<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="page-header-title">
                    <h5 class="m-b-10"></h5>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    @foreach(explode('.', Route::currentRouteName()) as $row)
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);"> {{ucwords($row)}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>