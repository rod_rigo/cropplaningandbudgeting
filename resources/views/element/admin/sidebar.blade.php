<nav class="pcoded-navbar">
    <div class="sidebar_toggle">
        <a href="javascript:void(0);">
            <i class="icon-close icons"></i
            ></a>
    </div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-80 img-radius" src="{{url('public/img/logo.png')}}" style="object-fit: contain !important;" loading="lazy" alt="Default Logo">
                <div class="user-details">
                    <span id="more-details">
                        {{strtoupper(auth()->user()->name)}}
                        <i class="fa fa-caret-down"></i>
                    </span>
                </div>
            </div>

            <div class="main-menu-content">
                <ul>
                    <li class="more-details">
                        <a href="user-profile.html">
                            <i class="ti-user"></i>
                            View Profile
                        </a>
                        <a href="{{route('users.logout')}}">
                            <i class="ti-layout-sidebar-left"></i>
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="p-15 p-b-0">

        </div>
        <div class="pcoded-navigation-label" data-i18n="nav.category.navigation">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="{{(request()->routeIs('admin.dashboards.index'))? 'active': null;}}">
                <a href="{{route('admin.dashboards.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.users.index'))? 'active': null;}}">
                <a href="{{route('admin.users.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-user"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Users</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.plants.index'))? 'active': null;}}">
                <a href="{{route('admin.plants.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-pagelines"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Plants</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.seeds.index'))? 'active': null;}}">
                <a href="{{route('admin.seeds.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-tree"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Seeds</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.supplies.index'))? 'active': null;}}">
                <a href="{{route('admin.supplies.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-package"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Supplies</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>

            <li class="{{(request()->routeIs('admin.units.index'))? 'active': null;}}">
                <a href="{{route('admin.units.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-balance-scale"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Units</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.categories.index'))? 'active': null;}}">
                <a href="{{route('admin.categories.index')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-list"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Categories</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>

        <div class="pcoded-navigation-label" data-i18n="nav.category.navigation">Bin</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="{{(request()->routeIs('admin.plants.bin'))? 'active': null;}}">
                <a href="{{route('admin.plants.bin')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-pagelines"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Plants</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.seeds.bin'))? 'active': null;}}">
                <a href="{{route('admin.seeds.bin')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-tree"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Seeds</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.supplies.bin'))? 'active': null;}}">
                <a href="{{route('admin.supplies.bin')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-package"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Supplies</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>

            <li class="{{(request()->routeIs('admin.units.bin'))? 'active': null;}}">
                <a href="{{route('admin.units.bin')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-balance-scale"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Units</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{(request()->routeIs('admin.categories.bin'))? 'active': null;}}">
                <a href="{{route('admin.categories.bin')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-list"></i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Categories</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </div>
</nav>