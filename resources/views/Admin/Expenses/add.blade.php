@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
            <a href="{{route('admin.supplies.index')}}" class="btn btn-primary rounded-0" title="Return">
                Return
            </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Expense Form</h5>
                </div>
                <form class="card-block" method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <label for="supply">Supply</label>
                            <input type="text" name="supply" class="form-control" id="supply" placeholder="Supply" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" id="price" placeholder="Price" pattern="([0,9]){1,}" title="{{ucwords('please fill out this field')}}" min="1" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="quantity">Quantity</label>
                            <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Quantity" pattern="([0,9]){1,}" title="{{ucwords('please fill out this field')}}" min="1" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="total">Total</label>
                            <input type="number" name="total" class="form-control" id="total" placeholder="Total" pattern="([0,9]){1,}" title="{{ucwords('please fill out this field')}}" min="1" autocomplete="off" readonly required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <label for="remarks">Remarks</label>
                            <textarea name="remarks" class="form-control" id="remarks" placeholder="Remarks" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required cols="30" rows="10">-</textarea>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="task">
                                <label for="task">Task</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <input type="hidden" name="is_task" id="is-task" value="0" required>
                            <input type="hidden" name="supply_id" id="supply-id" required>
                            <input type="hidden" name="balance" id="balance" value="0" required>
                            <input type="hidden" name="total_balance" id="total-balance" value="0" required>
                            <input type="hidden" name="plant_id" id="plant-id" value="{{$plant->id}}" required>
                            <input type="hidden" name="user_id" id="user-id" value="{{auth()->user()->id}}" required>
                            <button type="reset" class="btn btn-danger rounded-0 m-1">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-success rounded-0 m-1">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function (e) {

            var baseurl = window.location.href;

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('form-control-danger');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    swal('success', null, data.message);
                    Turbolinks.visit(data.redirect,{action: 'advance'});
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            $('#supply').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#price').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#quantity').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#total').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#remarks').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#task').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-task').val(Number(checked));
            });

            function computation() {
                var price = parseFloat($('#price').val());
                var quantity = parseFloat($('#quantity').val());
                var total = quantity * price;
                $('#total').val(total);
            }

            function getSuppliesList() {
                $.ajax({
                    url:mainurl+'supplies/getSuppliesList',
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend:function (e) {
                        $('#supply').prop('disabled', true);
                    },
                }).done(function (data, status, xhr) {
                    $('#supply').autocomplete({
                        delay: 100,
                        minLength: 2,
                        autoFocus: true,
                        source: function (request, response) {
                            response($.map(data, function (data, key) {
                                var name = data.supply.toUpperCase();
                                if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                    return {
                                        key: parseInt(data.id),
                                        label: data.supply,
                                        price: parseFloat(data.price),
                                    }
                                } else {
                                    return null;
                                }
                            }));
                        },
                        create: function (event, ui) {
                            $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                                var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                                return $('<li></li>')
                                    .data('item.autocomplete', item)
                                    .append('<li>'+(label)+'</li>')
                                    .appendTo(ul);
                            };
                        },
                        focus: function(e, ui) {
                            e.preventDefault();
                        },
                        select: function(e, ui) {
                            $('#supply').val(ui.item.label);
                            $('#supply-id').val(parseInt(ui.item.key));
                            $('#price, #total').val(parseFloat(ui.item.price));
                            $('#quantity').val(1);
                            computation();
                        },
                        change: function(e, ui ) {
                            e.preventDefault();
                        },
                    }).prop('disabled', false);
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
            }

            getSuppliesList();

        });
    </script>

@endsection