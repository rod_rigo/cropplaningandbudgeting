@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
            <a href="{{route('admin.supplies.index')}}" class="btn btn-primary rounded-0" title="Return">
                Return
            </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Supply Form</h5>
                </div>
                <form class="card-block" method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    <div class="row">

                        <div class="col-sm-12 col-md-8 col-lg-9 mt-3">
                            <label for="supply">Supply</label>
                            <input type="text" name="supply" class="form-control" id="supply" placeholder="Supply" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" id="price" placeholder="Price" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="0" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="unit-id">Unit</label>
                            <select name="unit_id" class="form-control" id="unit-id" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                                <option value="">Unit</option>
                                @foreach($units as $key => $value)
                                    <option value="{{intval($key)}}">{{ucwords($value)}}</option>
                                @endforeach
                            </select>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="category-id">Category</label>
                            <select name="category_id" class="form-control" id="category-id" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                                <option value="">Category</option>
                                @foreach($categories as $key => $value)
                                    <option value="{{intval($key)}}">{{ucwords($value)}}</option>
                                @endforeach
                            </select>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <label for="description">Description</label>
                            <textarea name="description" cols="30" rows="10" class="form-control" id="description" placeholder="Description" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off">-</textarea>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="active" checked>
                                <label for="active">Active</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <input type="hidden" name="is_active" id="is-active" value="1" required>
                            <input type="hidden" name="user_id" id="user-id" value="{{auth()->user()->id}}" required>
                            <button type="reset" class="btn btn-danger rounded-0 m-1">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-success rounded-0 m-1">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function (e) {

            var baseurl = window.location.href;
            var editor;

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('form-control-danger');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    swal('success', null, data.message);
                    Turbolinks.visit(data.redirect,{action: 'advance'});
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            CKEDITOR.ClassicEditor.create(document.getElementById('description'), {
                // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
                toolbar: {
                    items: [
                        'exportPDF','exportWord', '|',
                        'findAndReplace', 'selectAll', '|',
                        'heading', '|',
                        'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                        'bulletedList', 'numberedList', 'todoList', '|',
                        'outdent', 'indent', '|',
                        'undo', 'redo',
                        '-',
                        'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                        'alignment', '|',
                        'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                        'specialCharacters', 'horizontalLine', 'pageBreak',
                    ],
                    shouldNotGroupWhenFull: true
                },
                // Changing the language of the interface requires loading the language file using the <script> tag.
                // language: 'es',
                list: {
                    properties: {
                        styles: true,
                        startIndex: true,
                        reversed: true
                    }
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                        { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                        { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                        { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                        { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                    ]
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
                placeholder: 'Enter A Description Here...',
                // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
                fontFamily: {
                    options: [
                        'default',
                        'Arial, Helvetica, sans-serif',
                        'Courier New, Courier, monospace',
                        'Georgia, serif',
                        'Lucida Sans Unicode, Lucida Grande, sans-serif',
                        'Tahoma, Geneva, sans-serif',
                        'Times New Roman, Times, serif',
                        'Trebuchet MS, Helvetica, sans-serif',
                        'Verdana, Geneva, sans-serif'
                    ],
                    supportAllValues: true
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
                fontSize: {
                    options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                    supportAllValues: true
                },
                // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
                // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
                htmlSupport: {
                    allow: [
                        {
                            name: /.*/,
                            attributes: true,
                            classes: true,
                            styles: true
                        }
                    ]
                },
                // Be careful with enabling previews
                // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
                htmlEmbed: {
                    showPreviews: true
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
                link: {
                    decorators: {
                        addTargetToExternalLinks: true,
                        defaultProtocol: 'https://',
                        toggleDownloadable: {
                            mode: 'manual',
                            label: 'Downloadable',
                            attributes: {
                                download: 'file'
                            }
                        }
                    }
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
                mention: {
                    feeds: [
                        {
                            marker: '@',
                            feed: [
                                '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                                '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                                '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                                '@sugar', '@sweet', '@topping', '@wafer'
                            ],
                            minimumCharacters: 1
                        }
                    ]
                },
                // The "super-build" contains more premium features that require additional configuration, disable them below.
                // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
                removePlugins: [
                    // These two are commercial, but you can try them out without registering to a trial.
                    // 'ExportPdf',
                    // 'ExportWord',
                    'CKBox',
                    'CKFinder',
                    'EasyImage',
                    // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                    // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                    // Storing images as Base64 is usually a very bad idea.
                    // Replace it on production website with other solutions:
                    // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                    // 'Base64UploadAdapter',
                    'RealTimeCollaborativeComments',
                    'RealTimeCollaborativeTrackChanges',
                    'RealTimeCollaborativeRevisionHistory',
                    'PresenceList',
                    'Comments',
                    'TrackChanges',
                    'TrackChangesData',
                    'RevisionHistory',
                    'Pagination',
                    'WProofreader',
                    // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                    // from a local file system (file://) - load this site via HTTP server if you enable MathType
                    'MathType'
                ]
            }).then( function (data) {
                editor = data;
            }).catch(function (error) {
                window.location.reload();
            });

            setInterval(function(){
                editor.updateSourceElement();
            }, 1000);

            $('#supply').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#price').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#unit-id').on('input', function (e) {
                var value = $(this).val();

                if(!value){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#category-id').on('input', function (e) {
                var value = $(this).val();

                if(!value){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#active').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-active').val(Number(checked));
            });

        });
    </script>

@endsection