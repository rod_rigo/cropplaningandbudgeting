@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="text-c-purple" id="total-seeds">0</h4>
                            <h6 class="text-muted m-b-0">Total Seeds</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="fa fa-tree f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-purple">
                    <div class="row align-items-center">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="text-c-purple" id="total-supplies">0</h4>
                            <h6 class="text-muted m-b-0">Total Supplies</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="ti-package f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-purple">
                    <div class="row align-items-center">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="text-c-purple" id="total-plants">0</h4>
                            <h6 class="text-muted m-b-0">Total Plants</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="fa fa-pagelines f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-purple">
                    <div class="row align-items-center">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="text-c-purple" id="total-tasks">0</h4>
                            <h6 class="text-muted m-b-0">Total Tasks (Upcoming)</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="fa fa-pencil-square f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-purple">
                    <div class="row align-items-center">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <div id="calendar"></div>
        </div>
    </div>

    <script>
        'use strict';
        $(document).ready(function () {
            const baseurl = mainurl+'dashboards/';

            var calendar =  $('#calendar').evoCalendar({
                theme:'Midnight Blue',
                language:'en',
                todayHighlight:true,
                sidebarDisplayDefault:true,
                sidebarToggler:true,
                eventDisplayDefault:true,
                eventListToggler:true
            });

            function counts() {
                $.ajax({
                    url:baseurl+'counts',
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON',
                    beforeSend: function (e) {
                        $('#total-seeds, #total-supplies, #total-plants, #total-tasks').text(0);
                    },
                }).done(function (data, status, xhr) {
                    $('#total-seeds').text(parseFloat(data.seeds));
                    $('#total-supplies').text(parseFloat(data.supplies));
                    $('#total-plants').text(parseFloat(data.plants));
                    $('#total-tasks').text(parseFloat(data.tasks));
                    Swal.close();
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
            }

            function getTasks() {
                var array = [];
                $.ajax({
                    url:baseurl+'getTasks',
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON',
                    global:false,
                    async:false
                }).done(function (data, status, xhr) {
                    $.map(data, function (data) {
                        array.push({
                            id: data.id, // Event's id (required, for removing event)
                            name: data.plants.seeds.seed,
                            description: data.task, // Description of event (optional)
                            badge: ((data.is_complete)? 'Completed': 'Uncomplete'), // Event badge (optional)
                            date: data.start_at, // Date of event
                            type: 'Task', // Type of event (event|holiday|birthday)
                            color: '#448aff', // Event custom color (optional)
                            everyYear: false // Event is every year (optional)
                        });
                    });
                    Swal.close();
                }).fail(function (xhr, status, error) {
                    Swal.close();
                });
                return array;
            }

            counts();
            $('#calendar').evoCalendar('addCalendarEvent', getTasks());

        });
    </script>

@endsection