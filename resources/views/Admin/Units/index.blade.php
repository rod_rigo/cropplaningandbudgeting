@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-lg">
            <form id="form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                <div class="modal-header">

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8 mt-1">
                            <label for="unit">Unit</label>
                            <input type="text" name="unit" class="form-control" id="unit" placeholder="Unit" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-end mt-1">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="active" checked>
                                <label for="active">Active</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-end align-items-center">
                    <input type="hidden" name="is_active" id="is-active" value="1" required>
                    <input type="hidden" name="user_id" id="user-id" value="{{auth()->user()->id}}" required>
                    <button type="reset" class="btn btn-danger rounded-0 m-1">
                        Reset
                    </button>
                    <button type="submit" class="btn btn-success rounded-0 m-1">
                        Submit
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
            <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Unit">
                New Unit
            </button>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Units</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap mt-5" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        'use strict';
        $(document).ready(function () {
            const baseurl = mainurl+'units/';
            var url = '';
            var isActive = [
                'Inactive',
                'Active'
            ];
            var title = function () {
                return capitalize('unit reports');
            };

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                dom:'RlBfrtip',
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                pagingType: 'full_numbers',
                order:[[0, 'asc']],
                lengthMenu:[100, 200, 500, 1000],
                ajax:{
                    url:baseurl+'getUnits',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {

                    },
                    error:function (data, status, xhr) {
                        window.location.reload();
                    }
                },
                colReorder: {
                    allowReorder: true
                },
                buttons: [
                    {
                        extend: 'print',
                        title: 'Print',
                        text: 'Print',
                        attr:  {
                            id: 'print',
                            class:'btn btn-secondary rounded-0',
                        },
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '10px' )
                                .prepend('');
                            $(win.document.body).find( 'table tbody' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'excelHtml5',
                        attr:  {
                            id: 'excel',
                            class:'btn btn-success rounded-0',
                        },
                        title: title,
                        text: 'Excel',
                        tag: 'button',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'Excel',
                                text:'Export To Excel?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'pdfHtml5',
                        attr:  {
                            id: 'pdf',
                            class:'btn btn-danger rounded-0',
                        },
                        text: 'PDF',
                        title: title,
                        tag: 'button',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'PDF',
                                text:'Export To PDF?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        customize: function(doc) {
                            doc.pageMargins = [2, 2, 2, 2 ];
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.styles.tableHeader.fontSize = 7;
                            doc.styles.tableBodyEven.fontSize = 7;
                            doc.styles.tableBodyOdd.fontSize = 7;
                            doc.styles.tableFooter.fontSize = 7;
                        },
                        download: 'open',
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                ],
                columnDefs: [
                    {
                        targets: [0],
                        data: null,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: [1],
                        data: null,
                        render: function(data,type,row,meta){
                            return row.unit+ ' ('+(isActive[row.is_active])+')';
                        }
                    },
                    {
                        targets: [3],
                        data: null,
                        render: function(data,type,row,meta){
                            return moment(row.updated_at).format('Y-MM-DD hh:mm A');
                        }
                    },
                    {
                        targets: [4],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                                '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                        }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'unit'},
                    { data: 'users.name'},
                    { data: 'updated_at'},
                    { data: 'id'},
                ]
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'edit/'+(parseInt(dataId));
                $.ajax({
                    url:href,
                    type: 'GET',
                    method: 'GET',
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                        url = 'edit/'+(parseInt(dataId));
                        $('button[type="reset"]').fadeOut(100);
                    },
                }).done(function (data, status, xhr) {
                    $('#unit').val(data.unit);
                    $('#is-active').val(data.is_active);
                    $('#active').prop('checked', data.is_active);
                    $('#modal').modal('toggle');
                    Swal.close();
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });

            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+(parseInt(dataId));
                Swal.fire({
                    title: 'Delete Data',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function (e) {
                                Swal.fire({
                                    icon: 'info',
                                    title: null,
                                    text: 'Please Wait!...',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                        }).done(function (data, status, xhr) {
                            swal('success', null, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (data, status, xhr) {
                            swal('error', 'Error', data.responseJSON.message);
                        });
                    }
                });
            });

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl + url,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('is-invalid');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    $('#modal').modal('toggle');
                    table.ajax.reload(null, true);
                    swal('success', null, data.message);
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            $('#toggle-modal').click(function (e) {
                url = 'add';
                $('#modal').modal('toggle');
            });

            $('#modal').on('hidden.bs.modal', function (e) {
                $('#form')[0].reset();
                $('small').empty();
                $('.form-control').removeClass('form-control-danger');
                $('button[type="reset"]').fadeIn(100);
                $('button[type="reset"], button[type="submit"]').prop('disabled', false);
            });

            $('#unit').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#active').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-active').val(Number(checked));
            });

        });
    </script>

@endsection