@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Category</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap mt-5" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Category</th>
                                <th>Updated By</th>
                                <th>Deleted At</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        'use strict';
        $(document).ready(function () {
            const baseurl = mainurl+'categories/';
            var url = '';
            var isActive = [
                'Inactive',
                'Active'
            ];
            var title = function () {
                return capitalize('category reports Deleted');
            };

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                dom:'RlBfrtip',
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                pagingType: 'full_numbers',
                order:[[0, 'asc']],
                lengthMenu:[100, 200, 500, 1000],
                ajax:{
                    url:baseurl+'getCategoriesDeleted',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {

                    },
                    error:function (data, status, xhr) {
                        window.location.reload();
                    }
                },
                colReorder: {
                    allowReorder: true
                },
                buttons: [
                    {
                        extend: 'print',
                        title: 'Print',
                        text: 'Print',
                        attr:  {
                            id: 'print',
                            class:'btn btn-secondary rounded-0',
                        },
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '10px' )
                                .prepend('');
                            $(win.document.body).find( 'table tbody' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'excelHtml5',
                        attr:  {
                            id: 'excel',
                            class:'btn btn-success rounded-0',
                        },
                        title: title,
                        text: 'Excel',
                        tag: 'button',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'Excel',
                                text:'Export To Excel?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'pdfHtml5',
                        attr:  {
                            id: 'pdf',
                            class:'btn btn-danger rounded-0',
                        },
                        text: 'PDF',
                        title: title,
                        tag: 'button',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'PDF',
                                text:'Export To PDF?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        customize: function(doc) {
                            doc.pageMargins = [2, 2, 2, 2 ];
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.styles.tableHeader.fontSize = 7;
                            doc.styles.tableBodyEven.fontSize = 7;
                            doc.styles.tableBodyOdd.fontSize = 7;
                            doc.styles.tableFooter.fontSize = 7;
                        },
                        download: 'open',
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                ],
                columnDefs: [
                    {
                        targets: [0],
                        data: null,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: [1],
                        data: null,
                        render: function(data,type,row,meta){
                            return row.category+ ' ('+(isActive[row.is_active])+')';
                        }
                    },
                    {
                        targets: [3],
                        data: null,
                        render: function(data,type,row,meta){
                            return moment(row.deleted_at).format('Y-MM-DD hh:mm A');
                        }
                    },
                    {
                        targets: [4],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white restore" title="Restore">Restore</a> | '+
                                '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                        }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'category'},
                    { data: 'users.name'},
                    { data: 'deleted_at'},
                    { data: 'id'},
                ]
            });

            datatable.on('click','.restore',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'restore/'+(parseInt(dataId));
                Swal.fire({
                    title: 'Restore Data',
                    text: 'Are You Sure?',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'POST',
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function (e) {
                                Swal.fire({
                                    icon: 'info',
                                    title: null,
                                    text: 'Please Wait!...',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                        }).done(function (data, status, xhr) {
                            swal('success', null, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (data, status, xhr) {
                            swal('error', 'Error', data.responseJSON.message);
                        });
                    }
                });
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'forceDelete/'+(parseInt(dataId));
                Swal.fire({
                    title: 'Delete Data',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function (e) {
                                Swal.fire({
                                    icon: 'info',
                                    title: null,
                                    text: 'Please Wait!...',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                        }).done(function (data, status, xhr) {
                            swal('success', null, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (data, status, xhr) {
                            swal('error', 'Error', data.responseJSON.message);
                        });
                    }
                });
            });

        });
    </script>

@endsection