@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <script>
        var id = parseInt({{intval($plant->id)}});
    </script>

    <div class="modal fade" id="task-modal">
        <div class="modal-dialog modal-lg">
            <form id="task-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-1">
                                <label for="start-at">Start At</label>
                                <input type="date" name="start_at" class="form-control" id="start-at" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                                <small class="mt-2"></small>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                                <label for="task">Task</label>
                                <textarea name="task" cols="30" rows="10" class="form-control" id="task" placeholder="Task" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required></textarea>
                                <small class="mt-2"></small>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-end mt-1">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" id="complete">
                                    <label for="complete">Complete</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-end align-items-center">
                        <input type="hidden" name="is_complete" id="is-complete" value="1" required>
                        <input type="hidden" name="user_id" id="user-id" value="{{auth()->user()->id}}" required>
                        <input type="hidden" name="plant_id" id="plant-id" value="{{$plant->id}}" required>
                        <button type="reset" class="btn btn-danger rounded-0 m-1">
                            Reset
                        </button>
                        <button type="submit" class="btn btn-success rounded-0 m-1">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
            <a href="{{route('admin.expenses.add',['plantId' => intval($plant->id)])}}" class="btn btn-primary rounded-0 mx-1" title="New Task">
                New Expenses
            </a>
            <button type="button" id="task-toggle-modal" class="btn btn-primary rounded-0 mx-1" title="New Task">
                New Task
            </button>
            <a href="{{route('admin.plants.index')}}" class="btn btn-primary rounded-0" title="Return">
                Return
            </a>
        </div>
        <div class="col-sm-12 col-md-7 col-lg-8 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Plant Form</h5>
                </div>
                <form class="card-block" method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    <div class="row">

                        <div class="col-sm-12 col-md-9 col-lg-9 mt-3">
                            <label for="seed-id">Seed</label>
                            <select name="seed_id" class="form-control" id="seed-id" title="{{ucwords('please fill out this field')}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                                <option value="">Please Select Seed</option>
                                @foreach($seeds as $key => $value)
                                    <option value="{{intval($key)}}" {{(intval($key) == intval($plant->seed_id))? 'selected': null;}}>{{ucwords($value)}}</option>
                                @endforeach
                            </select>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                            <label for="quantity">Quantity</label>
                            <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Quantity" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="1" value="{{intval($plant->quantity)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="budget">Budget</label>
                            <input type="number" name="budget" class="form-control" id="budget" placeholder="Budget" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="0" value="{{doubleval($plant->budget)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" id="price" placeholder="Price" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="0" value="{{doubleval($plant->price)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="total">Total</label>
                            <input type="number" name="total" class="form-control" id="total" placeholder="Total" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="0" value="{{doubleval($plant->total)}}"  {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="harvest-at">Harvest At</label>
                            <input type="date" name="harvest_at" class="form-control" id="harvest-at" title="{{ucwords('please fill out this field')}}" autocomplete="off" value="{{date('Y-m-d', strtotime($plant->harvest_at))}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="harvested-at">Harvested At</label>
                            <input type="date" name="harvested-at" class="form-control" id="harvested-at" title="{{ucwords('please fill out this field')}}" autocomplete="off" disabled="">
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <label for="balance">Balance</label>
                            <input type="number" name="balance" class="form-control" id="balance" placeholder="Balance" pattern="([0-9]){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" min="0" value="{{doubleval($plant->balance)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <label for="description">Description</label>
                            <textarea name="description" cols="30" rows="10" class="form-control" id="description" placeholder="Description" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}}><?=$plant->description?></textarea>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="active" {{(boolval($plant->is_active))? 'checked': null;}} {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}}>
                                <label for="active">Active</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <input type="hidden" name="is_harvested" id="is-harvested" value="{{intval($plant->is_harvested)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <input type="hidden" name="is_active" id="is-active" value="{{intval($plant->is_active)}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            <input type="hidden" name="user_id" id="user-id" value="{{auth()->user()->id}}" {{(count($plant->tasks) || count($plant->expenses))? 'disabled': null;}} required>
                            @if(count($plant->tasks) || count($plant->expenses))
                            @else
                                <button type="reset" class="btn btn-danger rounded-0 m-1">
                                    Reset
                                </button>
                                <button type="submit" class="btn btn-success rounded-0 m-1">
                                    Submit
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4 mt-3">
            <div class="card table-card">
                <div class="card-header">
                    <h5>Tasks</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table id="task-datatable" class="table table-hover dt-responsive nowrap mt-3" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Task</th>
                                    <th>Start At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>Expenses</h5>
                </div>
                <div class="card-block">
                    <div class="table-responsive">
                        <table id="expense-datatable" class="table dt-responsive nowrap mt-3" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Supply</th>
                                <th>Balance</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Total Balance</th>
                                <th>Remarks</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Total</th>
                                <th></th>
                                <th>Current Total</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function (e) {

            var baseurl = mainurl+'plants/';
            var editor;

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl+'edit/'+(id),
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('form-control-danger');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    swal('success', null, data.message);
                    Turbolinks.visit(data.redirect,{action: 'advance'});
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            CKEDITOR.ClassicEditor.create(document.getElementById('description'), {
                // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
                toolbar: {
                    items: [
                        'exportPDF','exportWord', '|',
                        'findAndReplace', 'selectAll', '|',
                        'heading', '|',
                        'bold', 'italic', 'strikethrough', 'underline', 'code', 'subscript', 'superscript', 'removeFormat', '|',
                        'bulletedList', 'numberedList', 'todoList', '|',
                        'outdent', 'indent', '|',
                        'undo', 'redo',
                        '-',
                        'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                        'alignment', '|',
                        'link', 'insertImage', 'blockQuote', 'insertTable', 'mediaEmbed', 'codeBlock', '|',
                        'specialCharacters', 'horizontalLine', 'pageBreak',
                    ],
                    shouldNotGroupWhenFull: true
                },
                // Changing the language of the interface requires loading the language file using the <script> tag.
                // language: 'es',
                list: {
                    properties: {
                        styles: true,
                        startIndex: true,
                        reversed: true
                    }
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                        { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                        { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                        { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                        { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
                    ]
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
                placeholder: 'Enter A Description Here...',
                // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
                fontFamily: {
                    options: [
                        'default',
                        'Arial, Helvetica, sans-serif',
                        'Courier New, Courier, monospace',
                        'Georgia, serif',
                        'Lucida Sans Unicode, Lucida Grande, sans-serif',
                        'Tahoma, Geneva, sans-serif',
                        'Times New Roman, Times, serif',
                        'Trebuchet MS, Helvetica, sans-serif',
                        'Verdana, Geneva, sans-serif'
                    ],
                    supportAllValues: true
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
                fontSize: {
                    options: [ 10, 12, 14, 'default', 18, 20, 22 ],
                    supportAllValues: true
                },
                // Be careful with the setting below. It instructs CKEditor to accept ALL HTML markup.
                // https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html#enabling-all-html-features
                htmlSupport: {
                    allow: [
                        {
                            name: /.*/,
                            attributes: true,
                            classes: true,
                            styles: true
                        }
                    ]
                },
                // Be careful with enabling previews
                // https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html#content-previews
                htmlEmbed: {
                    showPreviews: true
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
                link: {
                    decorators: {
                        addTargetToExternalLinks: true,
                        defaultProtocol: 'https://',
                        toggleDownloadable: {
                            mode: 'manual',
                            label: 'Downloadable',
                            attributes: {
                                download: 'file'
                            }
                        }
                    }
                },
                // https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html#configuration
                mention: {
                    feeds: [
                        {
                            marker: '@',
                            feed: [
                                '@apple', '@bears', '@brownie', '@cake', '@cake', '@candy', '@canes', '@chocolate', '@cookie', '@cotton', '@cream',
                                '@cupcake', '@danish', '@donut', '@dragée', '@fruitcake', '@gingerbread', '@gummi', '@ice', '@jelly-o',
                                '@liquorice', '@macaroon', '@marzipan', '@oat', '@pie', '@plum', '@pudding', '@sesame', '@snaps', '@soufflé',
                                '@sugar', '@sweet', '@topping', '@wafer'
                            ],
                            minimumCharacters: 1
                        }
                    ]
                },
                // The "super-build" contains more premium features that require additional configuration, disable them below.
                // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
                removePlugins: [
                    // These two are commercial, but you can try them out without registering to a trial.
                    // 'ExportPdf',
                    // 'ExportWord',
                    'CKBox',
                    'CKFinder',
                    'EasyImage',
                    // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
                    // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
                    // Storing images as Base64 is usually a very bad idea.
                    // Replace it on production website with other solutions:
                    // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
                    // 'Base64UploadAdapter',
                    'RealTimeCollaborativeComments',
                    'RealTimeCollaborativeTrackChanges',
                    'RealTimeCollaborativeRevisionHistory',
                    'PresenceList',
                    'Comments',
                    'TrackChanges',
                    'TrackChangesData',
                    'RevisionHistory',
                    'Pagination',
                    'WProofreader',
                    // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
                    // from a local file system (file://) - load this site via HTTP server if you enable MathType
                    'MathType'
                ]
            }).then( function (data) {
                editor = data;
            }).catch(function (error) {
                window.location.reload();
            });

            setInterval(function(){
                editor.updateSourceElement();
            }, 1000);

            var select = $('#seed-id').select2({
                placeholder: 'Please Select Seed',
                allowClear: true,
                width: '100%'
            });

            select.on('select2:select', function (e) {
                var value = $(this).val();
                seeds(value);
            });

            $('#quantity').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#price').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#budget').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                computation();
                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#total').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#balance').on('input', function (e) {
                var value = $(this).val();
                var regex = /^([0-9]){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#harvest-at').change(function () {
                var value = $(this).val();

                if(!value){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#active').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-active').val(Number(checked));
            });

            function computation() {
                var quantity = parseFloat($('#quantity').val());
                var price = parseFloat($('#price').val());
                var total = quantity * price;

                var budget = parseFloat($('#budget').val());
                var balance = budget - total;

                $('#total').val(total);
                $('#balance').val(balance);
            }

            function seeds(id) {
                $.ajax({
                    url: mainurl+'seeds/view/'+(id),
                    type: 'GET',
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    var price = parseFloat(data.price);
                    var days = data.days;
                    var date = moment().add(parseFloat(days), 'day').format('Y-MM-DD');
                    $('#harvest-at').val(date);
                    $('#price, #total').val(price);
                    computation();
                    Swal.close();
                }).fail(function (data, status, xhr) {
                    Swal.close();
                });
            }

        });
        $(document).ready(function () {
            const baseurl = mainurl+'tasks/';
            var url = '';
            var isComplete = [
                null,
                'checked'
            ];
            var title = function () {
                return capitalize('task reports');
            };

            var datatable = $('#task-datatable');
            var table = datatable.DataTable({
                destroy:true,
                dom:'Rrt',
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                paging: false,
                ordering: false,
                pagingType: 'full_numbers',
                order:[[0, 'asc']],
                lengthMenu:[100, 200, 500, 1000],
                ajax:{
                    url:baseurl+'getTasks/'+(id),
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {

                    },
                    error:function (data, status, xhr) {
                        window.location.reload();
                    }
                },
                colReorder: {
                    allowReorder: true
                },
                buttons: [
                    {
                        extend: 'print',
                        title: 'Print',
                        text: 'Print',
                        attr:  {
                            id: 'print',
                            class:'btn btn-secondary rounded-0',
                        },
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '10px' )
                                .prepend('');
                            $(win.document.body).find( 'table tbody' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'excelHtml5',
                        attr:  {
                            id: 'excel',
                            class:'btn btn-success rounded-0',
                        },
                        title: title,
                        text: 'Excel',
                        tag: 'button',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'Excel',
                                text:'Export To Excel?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'pdfHtml5',
                        attr:  {
                            id: 'pdf',
                            class:'btn btn-danger rounded-0',
                        },
                        text: 'PDF',
                        title: title,
                        tag: 'button',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0,1,2,3,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'PDF',
                                text:'Export To PDF?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        customize: function(doc) {
                            doc.pageMargins = [2, 2, 2, 2 ];
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.styles.tableHeader.fontSize = 7;
                            doc.styles.tableBodyEven.fontSize = 7;
                            doc.styles.tableBodyOdd.fontSize = 7;
                            doc.styles.tableFooter.fontSize = 7;
                        },
                        download: 'open',
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                ],
                columnDefs: [
                    {
                        targets: [0],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            return '<div class="icheck-primary d-inline"> ' +
                                '<input type="checkbox" class="is-complete" id="complete-'+(row.id)+'" '+(isComplete[parseInt(row.is_complete)])+' data-id="'+(parseInt(row.id))+'"> ' +
                                '<label for="complete-'+(row.id)+'"></label> ' +
                                '</div>';
                        }
                    },
                    {
                        targets: [1],
                        data: null,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: [2],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            return '<div class="d-inline-block align-middle"> ' +
                                '<div class="d-inline-block"> <h6>'+(row.users.name)+'</h6> ' +
                                '<p class="text-muted m-b-0">'+(row.task)+'</p> ' +
                                '</div> ' +
                                '</div>';
                        }
                    },
                    {
                        targets: [4],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            var isComplete = (parseInt(row.is_complete))? '-':
                                '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete"><i class="ti-trash"></i></a>';
                            return isComplete;
                        }
                    }
                ],
                columns: [
                    { data: 'is_complete'},
                    { data: 'id'},
                    { data: 'task'},
                    { data: 'start_at'},
                    { data: 'id'},
                ]
            });

            datatable.on('change','.is-complete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var checked = Number($(this).prop('checked'));
                var href = baseurl+'isComplete/'+(parseInt(dataId))+'/'+(parseInt(checked));
                $.ajax({
                    url:href,
                    type: 'POST',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+(parseInt(dataId));
                Swal.fire({
                    title: 'Delete Data',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function (e) {
                                Swal.fire({
                                    icon: 'info',
                                    title: null,
                                    text: 'Please Wait!...',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                        }).done(function (data, status, xhr) {
                            swal('success', null, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (data, status, xhr) {
                            swal('error', 'Error', data.responseJSON.message);
                        });
                    }
                });
            });

            $('#task-form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl + url,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('is-invalid');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#task-form')[0].reset();
                    $('#task-modal').modal('toggle');
                    table.ajax.reload(null, true);
                    swal('success', null, data.message);
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            $('#task-toggle-modal').click(function (e) {
                url = 'add';
                $('#task-modal').modal('toggle');
            });

            $('#task-modal').on('hidden.bs.modal', function (e) {
                $('#form')[0].reset();
                $('small').empty();
                $('.form-control').removeClass('form-control-danger');
                $('button[type="reset"]').fadeIn(100);
                $('button[type="reset"], button[type="submit"]').prop('disabled', false);
            });

            $('#start-at').on('input', function (e) {
                var value = $(this).val();

                if(!value){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#task').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#complete').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-complete').val(Number(checked));
            });

        });
        $(document).ready(function () {
            const baseurl = mainurl+'expenses/';
            var url = '';
            var title = function () {
                return capitalize('Expense reports');
            };

            var datatable = $('#expense-datatable');
            var table = datatable.DataTable({
                destroy:true,
                dom:'RlBfrtip',
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                paging: true,
                ordering: true,
                pagingType: 'full_numbers',
                order:[[0, 'asc']],
                lengthMenu:[100, 200, 500, 1000],
                ajax:{
                    url:baseurl+'getExpenses/'+(id),
                    method: 'GET',
                    dataType: 'JSON',
                    beforeSend: function (e) {

                    },
                    error:function (data, status, xhr) {
                        window.location.reload();
                    }
                },
                colReorder: {
                    allowReorder: true
                },
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };

                    // Total over all pages
                    var total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    var pageTotal = api
                        .column(5, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(5).footer()).html(parseFloat(total).toFixed(2));
                    $(api.column(7).footer()).html('Page Total '+ pageTotal.toFixed(2));
                },
                buttons: [
                    {
                        extend: 'print',
                        title: 'Print',
                        text: 'Print',
                        attr:  {
                            id: 'print',
                            class:'btn btn-secondary rounded-0',
                        },
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6,7,8,9,]
                        },
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '10px' )
                                .prepend('');
                            $(win.document.body).find( 'table tbody' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'excelHtml5',
                        attr:  {
                            id: 'excel',
                            class:'btn btn-success rounded-0',
                        },
                        title: title,
                        text: 'Excel',
                        tag: 'button',
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6,7,8,9,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'Excel',
                                text:'Export To Excel?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                    {
                        extend: 'pdfHtml5',
                        attr:  {
                            id: 'pdf',
                            class:'btn btn-danger rounded-0',
                        },
                        text: 'PDF',
                        title: title,
                        tag: 'button',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0,1,2,3,4,5,6,7,8,9,]
                        },
                        action: function(e, dt, node, config) {
                            Swal.fire({
                                title:'PDF',
                                text:'Export To PDF?',
                                icon: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    setTimeout(function(){
                                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                    }, 1000);
                                }
                            });
                        },
                        customize: function(doc) {
                            doc.pageMargins = [2, 2, 2, 2 ];
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.styles.tableHeader.fontSize = 7;
                            doc.styles.tableBodyEven.fontSize = 7;
                            doc.styles.tableBodyOdd.fontSize = 7;
                            doc.styles.tableFooter.fontSize = 7;
                        },
                        download: 'open',
                        messageTop: function () {
                            return null;
                        },
                        messageBottom: function () {
                            return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                        },
                        footer:true
                    },
                ],
                columnDefs: [
                    {
                        targets: [0],
                        data: null,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: [2],
                        data: null,
                        render: function(data,type,row,meta){
                            return (new Intl.NumberFormat().format(parseFloat(row.balance)));
                        }
                    },
                    {
                        targets: [3],
                        data: null,
                        render: function(data,type,row,meta){
                            return (new Intl.NumberFormat().format(parseFloat(row.price)));
                        }
                    },
                    {
                        targets: [4],
                        data: null,
                        render: function(data,type,row,meta){
                            return (new Intl.NumberFormat().format(parseFloat(row.quantity)));
                        }
                    },
                    {
                        targets: [5],
                        data: null,
                        render: function(data,type,row,meta){
                            return (new Intl.NumberFormat().format(parseFloat(row.total)));
                        }
                    },
                    {
                        targets: [6],
                        data: null,
                        render: function(data,type,row,meta){
                            return (new Intl.NumberFormat().format(parseFloat(row.total_balance)));
                        }
                    },
                    {
                        targets: [9],
                        data: null,
                        render: function(data,type,row,meta){
                            return moment(row.updated_at).format('Y-MM-DD hh:mm A');
                        }
                    },
                    {
                        targets: [10],
                        data: null,
                        orderable:false,
                        searchable:false,
                        render: function(data,type,row,meta){
                            return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                        }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'supplies.supply'},
                    { data: 'balance'},
                    { data: 'price'},
                    { data: 'quantity'},
                    { data: 'total'},
                    { data: 'total_balance'},
                    { data: 'remarks'},
                    { data: 'users.name'},
                    { data: 'updated_at'},
                    { data: 'id'},
                ]
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+(parseInt(dataId));
                Swal.fire({
                    title: 'Delete Data',
                    text: 'Are You Sure?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend: function (e) {
                                Swal.fire({
                                    icon: 'info',
                                    title: null,
                                    text: 'Please Wait!...',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            },
                        }).done(function (data, status, xhr) {
                            swal('success', null, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (data, status, xhr) {
                            swal('error', 'Error', data.responseJSON.message);
                        });
                    }
                });
            });

        });
    </script>

@endsection