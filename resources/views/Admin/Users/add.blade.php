@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
            <a href="{{route('admin.users.index')}}" class="btn btn-primary rounded-0" title="Return">
                Return
            </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <h5>User Form</h5>
                </div>
                <form class="card-block" method="post" enctype="multipart/form-data" id="form">
                    @csrf
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control" id="username" placeholder="Username" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Password" pattern="(.){6,}" title="{{ucwords('please fill out this field at least 6 characters')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for="confirm-password">Password (Confirm)</label>
                            <input type="password" name="confirm_password" class="form-control" id="confirm-password" placeholder="Password (Confirm)" pattern="(.){6,}" title="{{ucwords('please fill out this field at least 6 characters')}}" autocomplete="off" required>
                            <small class="mt-2"></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="admin">
                                <label for="admin">Admin</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                            <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                            <input type="hidden" name="is_admin" id="is-admin" value="0" required>
                            <input type="hidden" name="is_active" id="is-active" value="1" required>
                            <button type="reset" class="btn btn-danger rounded-0 m-1">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-success rounded-0 m-1">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function (e) {

            var baseurl = window.location.href;

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('form-control-danger');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    swal('success', null, data.message);
                    Turbolinks.visit(data.redirect,{action: 'advance'});
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;

                    $.map(errors, function (data, key) {
                        $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                    });

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Swal.close();
                });
            });

            $('#name').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#username').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){1,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please fill out this field'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#email').on('input', function (e) {
                var value = $(this).val();
                var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('please enter valid email'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#password').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){6,}$/;

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('password must be at least 6 characters'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#confirm-password').on('input', function (e) {
                var value = $(this).val();
                var regex = /^(.){6,}$/;
                var password = $('#password').val();

                if(!value.match(regex)){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('confirm password must be at least 6 characters'));
                    return true;
                }

                if(value.toLowerCase() != password.toLowerCase()){
                    $(this).addClass('form-control-danger').next('small').text(capitalize('confirm password do not matched'));
                    return true;
                }

                $(this).removeClass('form-control-danger').next('small').empty();
            });

            $('#admin').change(function (e) {
                var checked = $(this).prop('checked');
                $('#is-admin').val(Number(checked));
            });

        });
    </script>

@endsection