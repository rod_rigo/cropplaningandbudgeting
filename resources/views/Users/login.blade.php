@extends('layout.default')
@section('title', ucwords(explode('.', Route::currentRouteName())[1]))
@section('content')

    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->

                    <form id="form" class="md-float-material form-material" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="text-center">
                            <img src="{{url('public/assets/images/logo.png')}}" loading="lazy" alt="logo.png">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Sign In</h3>
                                    </div>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="text" name="username" id="username" class="form-control" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" required>
                                    <span class="form-bar"></span>
                                    <label class="float-label">Email Or Username</label>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="password" id="password" class="form-control" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" required>
                                    <span class="form-bar"></span>
                                    <label class="float-label">Password</label>
                                </div>
                                <div class="row m-t-25 text-left">
                                    <div class="col-12">
                                        <div class="checkbox-fade fade-in-primary">
                                            <div class="icheck-primary d-inline">
                                                <input type="checkbox" name="remember_me" id="remember-me">
                                                <label for="remember-me">Remember Me</label>
                                            </div>
                                        </div>
                                        <div class="forgot-phone text-right f-right">
                                            <a href="javascript:void(0);" class="text-right f-w-600"> Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Sign in</button>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="{{url('public/assets/images/auth/Logo-small-bottom.png')}}" alt="small-logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>

    <script>
        $(document).ready(function (e) {

            var baseurl = window.location.href;

            $('#form').submit(function (e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    url: baseurl,
                    type: 'POST',
                    method: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                                $('small').empty();
                                $('.form-control').removeClass('form-control-danger');
                                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    $('#form')[0].reset();
                    swal('success', null, data.message);
                    Turbolinks.visit(data.redirect,{action: 'advance'});
                }).fail(function (data, status, xhr) {
                    const errors = data.responseJSON.errors;
                    var message = data.responseJSON.message;

                    if(errors){
                        $.map(errors, function (data, key) {
                            $('input[name="'+(key)+'"]').addClass('form-control-danger').next('small').text(data[0]);
                        });
                    }

                    if(message){
                        swal('error', null, message);
                    }

                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                });
            });

        });
    </script>

@endsection