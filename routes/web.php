<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['controller' => 'UsersController', 'prefix' => 'users', 'as' => 'users.'], function(){
    Route::redirect('/', 'users/login');
    Route::match(['get', 'post'],'login','login')->name('login');
    Route::get('logout','logout')->name('logout');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function (){

    Route::redirect('/','admin/dashboards/index');

    Route::group(['controller' => 'DashboardsController', 'prefix' => 'dashboards', 'as' => 'dashboards.'], function(){
        Route::redirect('/','dashboards/index');
        Route::get('index','index')->name('index');
        Route::get('counts','counts')->name('counts');
        Route::get('getTasks','getTasks')->name('getTasks');
    });

    Route::group(['controller' => 'UsersController', 'prefix' => 'users', 'as' => 'users.'], function (){
        Route::redirect('/','users/index');
        Route::get('index','index')->name('index');
        Route::get('getUsers','getUsers')->name('getUsers');
        Route::get('bin','bin')->name('bin');
        Route::get('getUsersDeleted','getUsersDeleted')->name('getUsersDeleted');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'SeedsController', 'prefix' => 'seeds', 'as' => 'seeds.'], function (){
        Route::redirect('/','seeds/index');
        Route::get('index','index')->name('index');
        Route::get('getSeeds','getSeeds')->name('getSeeds');
        Route::get('bin','bin')->name('bin');
        Route::get('getSeedsDeleted','getSeedsDeleted')->name('getSeedsDeleted');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}','view')->name('view');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'UnitsController', 'prefix' => 'units', 'as' => 'units.'], function (){
        Route::redirect('/','units/index');
        Route::get('index','index')->name('index');
        Route::get('getUnits','getUnits')->name('getUnits');
        Route::get('bin','bin')->name('bin');
        Route::get('getUnitsDeleted','getUnitsDeleted')->name('getUnitsDeleted');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'CategoriesController', 'prefix' => 'categories', 'as' => 'categories.'], function (){
        Route::redirect('/','categories/index');
        Route::get('index','index')->name('index');
        Route::get('getCategories','getCategories')->name('getCategories');
        Route::get('bin','bin')->name('bin');
        Route::get('getCategoriesDeleted','getCategoriesDeleted')->name('getCategoriesDeleted');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'SuppliesController', 'prefix' => 'supplies', 'as' => 'supplies.'], function (){
        Route::redirect('/','supplies/index');
        Route::get('index','index')->name('index');
        Route::get('getSupplies','getSupplies')->name('getSupplies');
        Route::get('bin','bin')->name('bin');
        Route::get('getSuppliesDeleted','getSuppliesDeleted')->name('getSuppliesDeleted');
        Route::get('getSuppliesList','getSuppliesList')->name('getSuppliesList');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'PlantsController', 'prefix' => 'plants', 'as' => 'plants.'], function (){
        Route::redirect('/','plants/index');
        Route::get('index','index')->name('index');
        Route::get('getPlants','getPlants')->name('getPlants');
        Route::get('bin','bin')->name('bin');
        Route::get('getPlantsDeleted','getPlantsDeleted')->name('getPlantsDeleted');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['get', 'post'],'edit/{id}','edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}','view')->name('view');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'TasksController', 'prefix' => 'tasks', 'as' => 'tasks.'], function (){
        Route::redirect('/','tasks/index');
        Route::get('getTasks/{plantId}','getTasks')->name('getTasks');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::post('isComplete/{id}/{isComplete}','isComplete')->name('isComplete');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
    });

    Route::group(['controller' => 'ExpensesController', 'prefix' => 'expenses', 'as' => 'expenses.'], function (){
        Route::redirect('/','expenses/index');
        Route::get('getExpenses/{plantId}','getExpenses')->name('getExpenses');
        Route::match(['get', 'post'],'add','add')->name('add');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
    });

});
